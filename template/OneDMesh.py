import numpy as np
import math
import matplotlib.pyplot as plt

class OneDMesh:
    def __init__(self, start, end, n_el, n_el_nodes, n_nodes, BC, A, E, b, t_bar, u_bar):
        self.start = start
        self.end = end
        self.n_el = n_el
        self.n_el_nodes = n_el_nodes
        self.n_nodes = n_nodes
        self.BC = BC
        self.A = A
        self.E = E
        self.b = b
        self.t_bar = t_bar
        self.u_bar = u_bar

        self.K = np.zeros([n_nodes, n_nodes])
        self.F = np.zeros(n_nodes)
        self.d = np.zeros(n_nodes)

        self.x_nodes = np.linspace(start, end, n_nodes)

        for i in range(n_nodes):
            self.F[i] += t_bar(self.x_nodes[i])

    def N(self, x, x_el):
        vals = np.ones(self.n_el_nodes)
        for i in range(self.n_el_nodes):
            for j in range(self.n_el_nodes):
                if j != i:
                    vals[i] *= (x - x_el[j]) / (x_el[i] - x_el[j])
        return vals

    def B(self, x, x_el):
        vals = np.zeros(self.n_el_nodes)
        for i in range(self.n_el_nodes):
            for j in range(self.n_el_nodes):
                if j != i:
                    build = 1 / (x_el[i] - x_el[j])
                    for k in range(self.n_el_nodes):
                        if k != i and k != j:
                            build *= (x - x_el[k]) / (x_el[i] - x_el[k])
                    vals[i] += build
        return vals

    def gauss_quad(self, start, end, n_gp, integrand):
        if n_gp == 1:
            xi = np.array([0])
            weights = np.array([2])
        elif n_gp == 2:
            xi = np.array([-1/(3**0.5), 1/(3**0.5)])
            weights = np.array([1, 1])
        elif n_gp == 3:
            xi = np.array([-(3/5)**0.5, 0, (3/5)**0.5])
            weights = np.array([5/9, 8/9, 5/9])
        elif n_gp == 5:
            xi = np.array([-(1/3)*(5-2*((10/17)**0.5))**0.5,
                           (1/3)*(5-2*((10/17)**0.5))**0.5,
                           -(1/3)*(5+2*((10/17)**0.5))**0.5,
                           (1/3)*(5+2*((10/17)**0.5))**0.5,
                           0])

            weights = np.array([(322 + 13*(70**0.5))/900,
                                (322 + 13*(70**0.5))/900,
                                (322 - 13*(70**0.5))/900,
                                (322 - 13*(70**0.5))/900,
                                128/225])

        else:
            print("Invalid input for linear quadrature. Gauss points set to 5 for accuracy.")
            n_gp = 5
            xi = np.array([-(1/3)*(5-2*((10/17)**0.5))**0.5,
                           (1/3)*(5-2*((10/17)**0.5))**0.5,
                           -(1/3)*(5+2*((10/17)**0.5))**0.5,
                           (1/3)*(5+2*((10/17)**0.5))**0.5,
                           0])

            weights = np.array([(322 + 13*(70**0.5))/900,
                                (322 + 13*(70**0.5))/900,
                                (322 - 13*(70**0.5))/900,
                                (322 - 13*(70**0.5))/900,
                                128/225])

        def x(xi):
            return (start+end) / 2 + xi * (end - start) /2

        val = 0 * integrand(0)
        J = (end - start) /2

        for i in range(n_gp):
            # j = J
            # ints = integrand(x(xi[i]))
            # w = weights[i]
            val += J * (integrand(x(xi[i])) * weights[i])

        return val

    def int_K(self, n_gp):
        for i in range(self.n_el):
            asm = np.arange(self.n_el_nodes) + int(i*(self.n_el_nodes-1))
            x_el = self.x_nodes[asm]

            def integrand(x):
                return self.A(x)*self.E(x)*np.outer(self.B(x, x_el).T, self.B(x, x_el))

            k_el = self.gauss_quad(x_el[0], x_el[-1], n_gp, integrand)
            for j in range(self.n_el_nodes):
                self.K[asm[j], asm] += k_el[j, :]
            # self.K[asm, asm] += self.gauss_quad(x_el[0], x_el[-1], n_gp, integrand)

    def int_F(self, n_gp):
        for i in range(self.n_el):
            asm = np.arange(self.n_el_nodes) + int(i*(self.n_el_nodes-1))
            x_el = self.x_nodes[asm]

            def integrand(x):
                return self.b(x)*self.N(x, x_el)

            self.F[asm] += self.gauss_quad(x_el[0], x_el[-1], n_gp, integrand)

    def solve_d(self):
        for i in range(self.n_nodes):
            if self.BC[i] == 1:
                self.F -= self.K[:, i] * self.u_bar(self.x_nodes[i])
                self.K[:, i] = 0
                self.K[i, :] = 0
                self.K[i, i] = 1

        for i in range(self.n_nodes):
            if self.BC[i] == 1:
                self.F[i] = self.u_bar(self.x_nodes[i])

        self.d = np.dot(np.linalg.inv(self.K), self.F)

    def u_solution(self, x):
        if x == self.x_nodes[-1]:
            el = self.n_el - 1
        else:
            el = math.floor((x-self.start)/((self.end-self.start)/self.n_el))

        # if el == 1:
        #     s=2

        nodes = np.arange(self.n_el_nodes) + int(el*(self.n_el_nodes-1))

        x_el = self.x_nodes[nodes]
        d_el = self.d[nodes]
        N_el = self.N(x, x_el)
        return np.dot(N_el, d_el)

    def du_dx_solution(self, x):
        if x == self.x_nodes[-1]:
            el = self.n_el - 1
        else:
            el = math.floor((x-self.start)/((self.end-self.start)/self.n_el))

        nodes = np.arange(self.n_el_nodes) + int(el*(self.n_el_nodes-1))

        x_el = self.x_nodes[nodes]
        d_el = self.d[nodes]
        B_el = self.B(x, x_el)
        return np.dot(B_el, d_el)

    def plot_u(self, title, res):
        x = np.linspace(self.start, self.end, res)
        u = np.zeros(res)

        for i in range(res):
            u[i] = self.u_solution(x[i])

        sol_plot = plt
        sol_plot.plot(x, u)
        sol_plot.grid()
        sol_plot.xlabel("x (m)")
        sol_plot.ylabel("displacement (m)")
        sol_plot.title(title)
        plt.show()

    def plot_stress(self, title, res):
        x = np.linspace(self.start, self.end, res)
        sig = np.zeros(res)

        for i in range(res):
            sig[i] = self.E(x[i])*self.du_dx_solution(x[i])

        sol_plot = plt
        sol_plot.plot(x, sig)
        sol_plot.grid()
        sol_plot.xlabel("x (m)")
        sol_plot.ylabel("stress (N/m^2)")
        sol_plot.title(title)
        plt.show()
