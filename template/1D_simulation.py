import numpy as np
import math
from OneDMesh import OneDMesh

n_el = 5
n_el_nodes = 2
n_nodes = (n_el_nodes-1) * n_el + 1
start = 0
end = 2


def A(x):
    return 1


def E(x):
    return 100


def b(x):
    return math.e ** (-x/2)


def t_bar(x):
    if x == 0:
        return 10*A(x)
    return 0


def u_bar(x):
    if x == 2:
        return 0
    return None


BC = np.zeros(n_nodes)      # 0 denotes Neumann, 1 Denotes Dirichelt
BC[-1] = 1


mesh = OneDMesh(start, end, n_el, n_el_nodes, n_nodes, BC, A, E, b, t_bar, u_bar)
mesh.int_F(5)
mesh.int_K(5)
mesh.solve_d()
# mesh.plot_u("ksjad", 100)
mesh.plot_stress("ksjad", 100)
